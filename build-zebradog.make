core = "7.x"
api = "2"
includes[] = http://cgit.drupalcode.org/sandbox-natemow-2445397/plain/drupal-org-core.make

; Profiles
projects[zebradog][download][type] = git
projects[zebradog][download][url] = http://git.drupal.org/sandbox/natemow/2445397.git
projects[zebradog][download][branch] = 7.x-1.x
projects[zebradog][type] = profile

