core = "7.x"
api = "2"

; Modules
; projects[admin_menu][subdir] = "contrib"
projects[admin_views][subdir] = "contrib"
projects[auto_nodetitle][subdir] = "contrib"
projects[block_class][subdir] = "contrib"
projects[color_field][subdir] = "contrib"
projects[conditional_fields][subdir] = "contrib"
projects[ckeditor][subdir] = "contrib"
projects[ctools][subdir] = "contrib"
projects[date][subdir] = "contrib"
projects[devel][subdir] = "contrib"
projects[entity][subdir] = "contrib"
projects[entityreference][subdir] = "contrib"
projects[features][subdir] = "contrib"
projects[field_collection][subdir] = "contrib"
projects[field_permissions][subdir] = "contrib"
projects[field_validation][subdir] = "contrib"
projects[file_entity][subdir] = "contrib"
projects[filefield_paths][subdir] = "contrib"
projects[fullcalendar][subdir] = "contrib"
projects[geolocation][subdir] = "contrib"
projects[jquery_update][subdir] = "contrib"
projects[libraries][subdir] = "contrib"
projects[module_filter][subdir] = "contrib"
projects[oauth][subdir] = "contrib"
projects[pathauto][subdir] = "contrib"
projects[rules][subdir] = "contrib"
projects[services][subdir] = "contrib"
;projects[services_oauth][subdir] = "contrib" is part of services
projects[token][subdir] = "contrib"
projects[transliteration][subdir] = "contrib"
projects[ultimate_cron][subdir] = "contrib"
projects[views][subdir] = "contrib"
projects[views_bulk_operations][subdir] = "contrib"
projects[views_datasource][subdir] = "contrib"
projects[views_php][subdir] = "contrib"

; Themes
projects[bootstrap][subdir] = "contrib"

; Libraries
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.7/ckeditor_4.4.7_standard.zip"
libraries[ckeditor][directory_name] = "ckeditor"

libraries[jquery_simple_color][download][type] = "get"
libraries[jquery_simple_color][download][url] = "https://github.com/recurser/jquery-simple-color/archive/master.zip"
libraries[jquery_simple_color][directory_name] = "jquery-simple-color"

libraries[fullcalendar][download][type] = "get"
libraries[fullcalendar][download][url] = "https://github.com/arshaw/fullcalendar/releases/download/v1.6.7/fullcalendar-1.6.7.zip"
libraries[fullcalendar][directory_name] = "fullcalendar"

libraries[spyc][download][type] = "get"
libraries[spyc][download][url] = "https://github.com/mustangostang/spyc/archive/master.zip"
libraries[spyc][directory_name] = "spyc"

