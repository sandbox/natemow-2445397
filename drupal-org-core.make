core = "7.x"
api = "2"

projects[drops-7][type] = "core"
projects[drops-7][download][type] = "get"
projects[drops-7][download][url] = "https://github.com/pantheon-systems/drops-7/archive/master.zip"
